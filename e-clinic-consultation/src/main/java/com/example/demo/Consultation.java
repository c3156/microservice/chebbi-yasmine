package com.example.demo;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;



@Entity
@Table(name = "Consultation")
public class Consultation {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer idConsultation;

	
	
	@Column(name ="Date ")
	public Date date ;
	
	@ManyToOne
	private  Doctor doctor ;
	
	@ManyToOne
	private  Patient patient ;
	
	public Date getDate() {
		return (date);
	}

	public void updateConsultation(Consultation consultation) {
		this.idConsultation = idConsultation;
		this.date = date;
		/*this.idDoctor = idDoctor ;*/
		
	}
	
}

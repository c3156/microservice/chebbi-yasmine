package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public class ConsultationControllor {
	@Autowired
	private ConsultationRepository consultationRepository;

	
	@PostMapping("/")
	public Consultation createConsultation(@RequestBody Consultation consultation) {
		Consultation consultation2 = consultationRepository.save(consultation);
		return consultation2 ;
	}
	
	@GetMapping("/")
	public List<Consultation> getConsultations() {
		return consultationRepository.findAll();
	}
	
	@GetMapping("/{id}")
	public Consultation getConsultation(@PathVariable Integer id) {
		return consultationRepository.findById(id).get();
	}
	
	@PutMapping("/{id}")
	public Consultation updateConsultation(@PathVariable Integer id,@RequestBody Consultation consultation ) {
		Consultation consultation2 = consultationRepository.findById(id).get();
		consultation2.updateConsultation(consultation);
		return consultationRepository.save(consultation2);
	}

	
	@DeleteMapping("/")
	public String deleteConsultation(@RequestBody Consultation consultation) {
		consultationRepository.delete(consultation);
		return "deleted!!";
	}

}

package com.example.demo;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "Doctor")
public class Doctor {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer idDoctor;
	
	@Column(name = "nom")
	public String nom;
	@Column(name = "prenom")
	public String prenom;
	@Column(name = "specialite")
	public String specialite;
	@ManyToOne
	private Patient patient;
	
	
	/*@OneToMany(mappedBy ="doctor")
   private List<ConsultationId> Consultation;*/
	
	public Doctor() {
		super();
	}

	public Doctor(Integer idDoctor, String nom,String prenom , String specialite ) {
		super();
		this.idDoctor = idDoctor;
		this.nom = nom;
		this.prenom = prenom ;
		this.specialite = specialite ;
	}

	
	
	public Integer getId() {
		return idDoctor;
	}

	public void setId (Integer id) {
		this.idDoctor = id;
	}
	
	public String getNom() {
		return (nom);
	}
	
	
	public void setNom(String name) {
		nom = name;
	}

	public String getPrenom() {
		return (prenom);
	}
	public String getSpecialite() {
		return (specialite);
	}


	public void setPrenom(String pre) {
		prenom = pre;
	}
	public void setSpecialite(String spec) {
		specialite = spec;
	}

	@Override
	public String toString() {
		return "Patient  [id=" + idDoctor  + ", nom=" + nom + ", prenom=" + prenom + ", specialite=" + specialite +  "]";
	}

	public void updateDoctor(Doctor doctor) {
		this.idDoctor = idDoctor;
		this.nom = nom;
		this.prenom = prenom ;
		this.specialite = specialite ;
	}
}

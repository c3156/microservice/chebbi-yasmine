package com.example.demo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "Patient")
public class Patient {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer idPatient;
	
	@Column(name = "nom")
	public String nom;
	@Column(name = "prenom")
	public String prenom;
	
	/*@OneToMany(mappedBy ="patient")
	private List<ConsultationId> Consultation;*/
	
	
	public Integer getId() {
		return idPatient;
	}

	public void setId (Integer id) {
		this.idPatient = id;
	}
	
	public String getNom() {
		return (nom);
	}
	
	
	public void setNom(String name) {
		nom = name;
	}

	public String getPrenom() {
		return (prenom);
	}

	public void setPrenom(String pre) {
		prenom = pre;
	}

	@Override
	public String toString() {
		return "Patient  [id=" + idPatient + ", nom=" + nom + ", prenom=" + prenom +  "]";
	}
	public void updateDoctor(Doctor doctor) {
		this.idPatient = idPatient;
		this.nom = nom;
		this.prenom = prenom ;
		
	}
}


package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/doctors")
public class DoctorController {
	@Autowired
	private DoctorRepository doctorRepository;

	
	@PostMapping("/doctors")
	public Doctor createDoctor(@RequestBody Doctor doctor) {
		Doctor doctor2 = doctorRepository.save(doctor);
		return doctor2;
	}
	
	@GetMapping("/doctors")
	public List<Doctor> getDoctors() {
		return doctorRepository.findAll();
	}
	
	@GetMapping("/doctors/{id}")
	public Doctor getDoctor(@PathVariable Integer id) {
		return doctorRepository.findById(id).get();
	}
	
	@PutMapping("/doctors/{id}")
	public Doctor updateDoctor(@PathVariable Integer id,@RequestBody Doctor doctor ) {
		Doctor doctor2 = doctorRepository.findById(id).get();
		doctor2.updateDoctor(doctor);
		return doctorRepository.save(doctor2);
	}

	
	@DeleteMapping("/doctors")
	public String deleteDoctor(@RequestBody Doctor doctor) {
		doctorRepository.delete(doctor);
		return "deleted!!";
	}
	//get doctor by specialite 
	
	@GetMapping("/specialite/{specialite}")
	public List<Doctor> findByspecialite(@PathVariable("specialite") String specialite) {
		
			return doctorRepository.findByspecialite(specialite);
}

	
}

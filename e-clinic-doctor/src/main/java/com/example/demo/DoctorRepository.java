package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
@Repository 

public interface DoctorRepository extends JpaRepository<Doctor,Integer>{

	List<Doctor>doctors = new ArrayList<>();
	
	public default  List<Doctor> findByspecialite(String specialite){
		return doctors.stream().filter(a -> a.getSpecialite().equals(specialite)).toList();
	}


}

package com.example.demo;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import antlr.collections.List;
@FeignClient(name = "AvailableDoctors")
public interface DoctorConsumer {
	@GetMapping("/specialite/{specialite}")
	List findByspecialite(@PathVariable("specialite") String specialite);

	
}

package com.example.demo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController 
@RequestMapping("/api/eclinic")
public class PatientController {
	@Autowired
	private PatientRepository patientRepository;

	
	@PostMapping("/patients")
	public Patient createPatient(@RequestBody Patient patient) {
		Patient patient2 = patientRepository.save(patient);
		return patient2 ;
	}
	
	@GetMapping("/patients")
	public List<Patient> getPatients() {
		return patientRepository.findAll();
	}
	
	@GetMapping("/patients/{id}")
	public Patient getPatient(@PathVariable Integer id) {
		return patientRepository.findById(id).get();
	}
	
	@PutMapping("/patients/{id}")
	public Patient updatePatient(@PathVariable Integer id,@RequestBody Patient patient ) {
		Patient patient2 = patientRepository.findById(id).get();
		patient2.updatePatient(patient);
		return patientRepository.save(patient2);
	}

	
	@DeleteMapping("/patients")
	public String deletePatient(@RequestBody Patient patient) {
		patientRepository.delete(patient);
		return "deleted!!";
	}
	
	

}

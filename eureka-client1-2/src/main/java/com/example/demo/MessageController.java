package com.example.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {
	
	@Autowired
	MessageConsummer consummer;
	
	@RequestMapping("/message")
	public String getMessage() {
		return  consummer.getMessage()+ "  en genie logiciel";
	}
}

